
rs_list = [69]#range(1,69,1)#[4.0,69.0]#[1.0,2.0,3.0,4.0,10.0,20.0,30.0,40.0,50.0,60.0,69.0,100.0]#

routine = 'eps_c' # moments spectral third_moment contour_data eps_c kram_kron

ncore = 4
savedir = './'

fxc = 'MCP07' # 'MCP07' 'MCP07_static' 'RPA' 'ALDA' MCP07_inf
new_hx = False
epsilon = 'C'

int_pars = {
'grid_type' : 'GLEG', # GLEG, trapezoid
'grid_pts': 2000, # for GLEG
'step' : 1.e-5, # for trapezoid
'diff': 1.0e-8,
'q_max': 3.0,
'n_q_pts': 300,
'min_cutoff': 2,
'max_cutoff': 500,
}

spec_pars = {
'plot' : True,
'save_plot': True,
'font_size': 16
}

m3_pars = {
'spectral': 'tddft', # tddft, qmc
'adaptive': 'global', # local, global, mixed
'min_recur': 2, # minimum number of starting region partitions
'max_recur': 2000,
'save': False,
'min_grid_size': 5,
'max_grid_size': 2, # maximum permitted mesh density increases
'precision' : 1.e-7,
'interp_grid': 0.005,
'use_interp': True
}

# use parameterized analytic continuation of GKI kernel to imaginary frequencies?
im_omega_param = True

kram_kron_pars = {
'x_only': False
}

if new_hx:
    kc_d = {4.0: 2.89, 69.0: 3.92}
else:
    kc_d = {4.0: 2.97, 69.0: 3.92}#6.65}#
