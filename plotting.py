import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate
from matplotlib import cm,colors,ticker
from lsda import ec_pz81

fsz = 24

clist=['tab:blue','tab:orange']
base_str = '/Users/aaronkaplan/Dropbox/phd.nosync/plasmons/new_routines/data_in_paper'
mkrlist=['o','s','d','^','v','x','*','+']

def plots_2d():
    ref_str = {'4': base_str+'/../../q-vs-wp-rs-4.dat','69': base_str+'/../../q-vs-wp-rs-69.dat'}

    dat_str = {'4': base_str+'/4.0_moments.csv','69': base_str+'/69.0_moments.csv'}
    m3_sr = {'4': base_str+'/rs_4.0_third_moment_sum_rule.csv','69': base_str+'/rs_69.0_third_moment_sum_rule.csv'}
    #var_dat_str = {'4': base_str+'/var_omega_rs_4.csv','69': base_str+'/var_omega_rs_69.csv'}

    q = {}
    o_std = {}
    ts_d = {}



    for jjj in range(2):
        fig,ax = plt.subplots(figsize=(8,6))

        for iter,rs in enumerate(ref_str):
            tq,ts,_,to,_,too,_ = np.transpose(np.genfromtxt(dat_str[rs],delimiter=',',skip_header=1))
            wp = (3.0/float(rs)**3)**(0.5)
            kf = (9*np.pi/4.0)**(1.0/3.0)/float(rs)
            lstl='--'
            if jjj == 1:
                lstl = '-'
            ax.plot(tq,to/ts,color=clist[iter],linestyle=lstl,linewidth=2)
            mods = ''
            if jjj ==1:
                mods = '_w_sum_rule'
                ax.plot(tq,(kf*tq)**2/(2.0*ts*wp),color=clist[iter],linestyle='--',linewidth=2)
                np.savetxt(base_str+'/sum_rule_rs_'+rs+'.csv',np.transpose((tq,to*wp,(kf*tq)**2/2.0,to*wp-(kf*tq)**2/2.0)),header='q,<omega_p(q)>,q**2/2,diff',delimiter=',')
            q[rs] = tq
            o_std[rs] = (too/ts-(to/ts)**2)**(0.5)
            ts_d[rs] = ts
            #ax.scatter(tq,to,color=clist[iter],s=10,marker='o')
            tq,to = np.transpose(np.genfromtxt(ref_str[rs],delimiter=''))
            if rs == '69' and jjj == 0:
                ax.plot(tq,to/wp,color=clist[iter],linestyle='-',linewidth=2)
            ax.annotate('$r_s='+rs+'$',(tq[-1],to[-1]/wp+.05),fontsize=fsz)


        ax.set_xlabel('$q/k_F$',fontsize=fsz)
        ax.set_xlim([0.0,3.0])
        ax.tick_params(axis='both',labelsize=fsz*.8)
        ax.set_ylabel('$\langle \omega_p(q)\\rangle/\omega_p(0)$',fontsize=fsz)
        plt.savefig(base_str+'/omega_avg_q'+mods+'.pdf',dpi=300,bbox_inches='tight')

        #plt.show()

        plt.clf()
        plt.cla()
        plt.close(fig='all')

    fig,ax = plt.subplots(figsize=(8,6))
    for iter,rs in enumerate(dat_str):
        #tq,to = np.transpose(np.genfromtxt(var_dat_str[rs],delimiter=','))
        ax.plot(q[rs],o_std[rs],color=clist[iter],linestyle='-',linewidth=2,markersize=0,marker='o')
        ax.annotate('$r_s='+rs+'$',(q[rs][int(np.ceil(len(q[rs])/2.0))],o_std[rs][int(np.ceil(len(o_std[rs])/2.0))]-.1),fontsize=fsz)

    ax.set_xlabel('$q/k_F$',fontsize=fsz)
    ax.set_xlim([0.0,3.0])
    ax.tick_params(axis='both',labelsize=fsz*.8)
    ax.set_ylabel('$\langle \Delta \omega_p(q) \\rangle/\omega_p(0)$',fontsize=fsz)
    plt.savefig(base_str+'/omega_var_q.pdf',dpi=300,bbox_inches='tight')

    plt.clf()
    plt.cla()
    plt.close(fig='all')

    max_s = np.asarray([ts_d['4'].max(),ts_d['69'].max()]).max()
    fig,ax = plt.subplots(figsize=(8,6))
    for iter,rs in enumerate(dat_str):
        #tq,to = np.transpose(np.genfromtxt(var_dat_str[rs],delimiter=','))
        ax.plot(q[rs],ts_d[rs],color=clist[iter],linestyle='-',linewidth=2,markersize=0,marker='o')
        if rs == '4':
            x_an = 0.3
            y_an = 1.1
        elif rs == '69':
            x_an = 1.5
            y_an = 5.0
        ax.annotate('$r_s='+rs+'$',(x_an,y_an),fontsize=fsz)

    #tq,ts,_,to,_,too,_ = np.transpose(np.genfromtxt(base_str+'/omega_rs_4_simo.csv',delimiter=',',skip_header=1))
    #ax.plot(tq/2.0+.01,ts,color='tab:blue',linestyle='-.',linewidth=1)
    #tq,ts,_,to,_,too,_ = np.transpose(np.genfromtxt(base_str+'/omega_rs_69_simo.csv',delimiter=',',skip_header=1))
    #ax.plot(tq/2.0+.01,ts,color='tab:orange',linestyle='-.',linewidth=1)


    ax.plot(np.asarray([0.0,q[rs].max()]),np.asarray([1.0,1.0]),linestyle='--',linewidth=1.5,markersize=0,c='tab:gray')
    #ax.plot(np.asarray([1.07,1.07]),np.asarray([0.0,1.05*max_s]),linestyle='--',linewidth=1.5,markersize=0,c='tab:gray')
    ax.set_xlabel('$q/k_F$',fontsize=fsz)
    ax.set_xlim([0.0,3.0])
    ax.set_ylim([0.0,1.05*max_s])
    ax.tick_params(axis='both',labelsize=fsz*.8)
    ax.set_ylabel('$S(q)$',fontsize=fsz)
    plt.savefig(base_str+'/s_q.pdf',dpi=300,bbox_inches='tight')

    #plt.show()
    plt.clf()
    plt.cla()
    plt.close(fig='all')

    fig,ax = plt.subplots(figsize=(8,6))
    for iter,rs in enumerate(m3_sr):
        qq,m3,sr,errs = np.transpose(np.genfromtxt(m3_sr[rs],delimiter=',',skip_header=1))
        ax.plot(qq,m3,color=clist[iter],linestyle='-',linewidth=2,markersize=0,marker='o')
        if rs == '4':
            fac = 6.0
            ax.plot(qq,sr,color=clist[iter],linestyle='--',linewidth=2,markersize=0,marker='o')
        elif rs == '69':
            fac = 0.3
            qq = qq[sr>0.0]
            errs = errs[sr>0.0]
            sr = sr[sr>0.0]
            ax.plot(qq,sr,color=clist[iter],linestyle='--')#,marker='o'
            #ax.plot(qq,sr,color=clist[iter],linestyle='--',linewidth=2,markersize=0,marker='o')
        ax.annotate('$r_s='+rs+'$',(qq[int(np.ceil(len(qq)/2.0))],m3[int(np.ceil(len(m3)/2.0))]*fac),fontsize=fsz)
    plt.yscale('log')
    ax.set_yticks([10**i for i in range(-5,3)])
    locmin = ticker.LogLocator(base=10.0,subs=np.arange(0.1,1,0.1),numticks=12)
    ax.yaxis.set_minor_locator(locmin)
    ax.yaxis.set_minor_formatter(ticker.NullFormatter())
    ax.set_xlabel('$q/k_F$',fontsize=fsz)
    ax.set_xlim([0.0,3.0])
    ax.tick_params(axis='both',labelsize=fsz*.8)
    ax.set_ylabel('[LHS or RHS of Eq. S1]$/[\omega_p(0)]^3$',fontsize=fsz)
    plt.savefig(base_str+'/m3_sr.pdf',dpi=300,bbox_inches='tight')
    #plt.show()

    plt.clf()
    plt.cla()

    return

def eps_c_plots():
    ncl=['tab:blue','tab:orange','tab:green','tab:red','tab:purple','tab:brown','tab:olive','tab:gray']
    nbs = base_str+'/eps_c_data_28_december_2020/'
    fls = {'MCP07': nbs+ 'epsilon_C_MCP07.csv',
    'static MCP07': nbs+ 'epsilon_C_MCP07_static.csv',
    'RPA': nbs+ 'epsilon_C_RPA.csv',
    'ALDA': nbs+ 'epsilon_C_ALDA.csv',
    #'MCP07 $\omega\\to \infty$': nbs+'epsilon_C_MCP07_inf.csv',
    #'MCP07 undamped':nbs+'epsilon_C_MCP07_undamp.csv'
     }
    label = {'MCP07': (68.82,0.001),
    'static MCP07': (39.73,0.001),
    'RPA': (71.66,-0.015),
    'ALDA': (30,0.01),
    'PZ81': (14,-0.02),#(85,-0.002)
    'MCP07 $\omega\\to \infty$': (82,.001),
    'MCP07 undamped': (25.4,-0.015)
    }
    fig,ax = plt.subplots(figsize=(10,6))
    for ifxc,fxc in enumerate(fls):
        rs,ec,_,_ = np.transpose(np.genfromtxt(fls[fxc],delimiter=',',skip_header=1))
        ax.plot(rs,ec,color=ncl[ifxc],linewidth=1.5,label=fxc)#,marker=mkrlist[ifxc])
        ax.annotate(fxc,label[fxc],color=ncl[ifxc],fontsize=16)
    rsl = np.linspace(0.5,100.0,2000)
    ax.set_xlim([1.0,101])
    ax.hlines(0.0,plt.xlim()[0],plt.xlim()[1],linestyle='-',color='black')
    ax.plot(rsl,ec_pz81(rsl,0.0),color='black',linestyle='--',label='PZ81')
    ax.annotate('PZ81',label['PZ81'],color='black',fontsize=16)
    #plt.xscale('log')
    #ax.legend()
    ax.set_ylabel('$\\varepsilon_{\mathrm{c}}(r_s)$ (Hartree/electron)',fontsize=20)
    ax.set_xlabel('$r_s$ (Bohr)',fontsize=20)
    ax.tick_params(axis='both',labelsize=16)
    #plt.show()
    plt.savefig(base_str+'/eps_c_comparison.pdf',dpi=600,bbox_inches='tight')
    return

def spectral_contour_plots():

    dat_str = {'4': base_str+'/spectral_rs_4.0.csv','69': base_str+'/spectral_rs_69.0.csv'}

    dat_str['69_log'] =dat_str['69']
    #npts = 200

    for irs,rs in enumerate(dat_str):

        fig,ax = plt.subplots(figsize=(8,6))

        q,omega,s = np.transpose(np.genfromtxt(dat_str[rs],delimiter=',',skip_header=1))
        if irs == 0:
            npts = int(np.ceil((len(q)*len(omega))**(0.25)))
        if rs =='69_log':
            s = np.log10(s)
        qt = np.linspace(q.min(),q.max(),npts)
        omegat = np.linspace(omega.min(),omega.max(),npts)


        if rs == '4':
            step_sz = 2
        elif rs == '69':
            step_sz = 20
        elif rs == '69_log':
            step_sz = 0.5
        st = scipy.interpolate.griddata((q,omega),s,(qt[None,:], omegat[:,None]),method='cubic')

        if rs == '69_log':
            cts = np.arange(np.floor(s.min()),np.floor(np.asarray([s.max(),40.0]).min()),step_sz)
        elif rs == '69':
            cts = np.arange(np.floor(s.min()),np.ceil(np.asarray([s.max(),4000.0]).min()),step_sz)
        else:
            cts = np.arange(np.floor(s.min()),np.ceil(np.asarray([s.max(),40.0]).min()),step_sz)
        cml = cm.get_cmap('Spectral')
        ccl = cml(np.linspace(.95,0.0,cts.shape[0]))
        ncml = colors.ListedColormap(ccl)
        ncml.set_over('white')#ccl[-1])
        ncml.set_under('purple')#ccl[0])

        cplt=ax.contourf(qt,omegat,st,cts,cmap=ncml,extend='both')
        ax.set_aspect('equal')
        ax.set_xlabel('$q/k_F$',fontsize=fsz)
        ax.set_xticks(np.arange(0.0,3.01,0.5))
        ax.set_ylabel('$\omega/\omega_p(0)$',fontsize=fsz)
        ax.tick_params(axis='both',labelsize=fsz*.8)
        fig.colorbar(cplt,extend='both',shrink=0.8)
        if rs == '4':
            plt.title('$S(q,\omega)$',fontsize=fsz)
        elif rs == '69':
            plt.title('$S(q,\omega)$',fontsize=fsz)
        elif rs == '69_log':
            plt.title('$\log_{10}[S(q,\omega)]$',fontsize=fsz)

        plt.savefig(base_str+'/spectral_contour_rs_'+rs+'.pdf',dpi=300,bbox_inches='tight')
        plt.cla()
        plt.clf()

    return


def dielectric_contour_plots():

    dat_str = {'4': base_str+'/dielectric_rs_4.0',
    '69': base_str+'/dielectric_rs_69.0'}


    for irs,rs in enumerate(dat_str):

        fig,axs = plt.subplots(2,2,figsize=(10,10))

        for ifreq,freq in enumerate(['re','im']):

            q,omega,eps_re,eps_im = np.transpose(np.genfromtxt(dat_str[rs]+'_'+freq+'_omega.csv',delimiter=',',skip_header=1))

            if ifreq==0:
                npts = int(np.ceil((len(q)*len(omega))**(0.25)))
                min_c=-40.0#np.asarray([eps_re.min(),-40.0]).max()
                max_c =40.01 #np.asarray([eps_re.max(),40.01]).min()

            qt = np.linspace(q.min(),q.max(),npts)
            omegat = np.linspace(omega.min(),omega.max(),npts)

            for ipart,eps in enumerate([eps_re,eps_im]):

                epst = scipy.interpolate.griddata((q,omega),eps,(qt[None,:], omegat[:,None]),method='cubic')
                #if rs == '4':
                step_sz = 4
                #elif rs == '69':
                #    step_sz = 0.5

                cts = np.arange(np.floor(min_c),np.ceil(max_c),step_sz)
                cml = cm.get_cmap('Spectral')
                ccl = cml(np.linspace(.95,0.0,cts.shape[0]))
                ncml = colors.ListedColormap(ccl)
                ncml.set_over('white')#ccl[-1])
                ncml.set_under('purple')#ccl[0])


                cplt=axs[ifreq,ipart].contourf(qt,omegat,epst,cts,cmap=ncml,extend='both')
                axs[ifreq,ipart].contour(cplt,levels=[0.0],colors='black',linestyles='--')
                axs[ifreq,ipart].set(aspect='equal')
                if ifreq == 1:
                    axs[ifreq,ipart].set_xlabel('$q/k_F$',fontsize=fsz)
                axs[ifreq,ipart].set_xticks(np.arange(0.0,3.01,1.0))
                axs[ifreq,ipart].set_yticks(np.arange(0.0,np.ceil(omega.max()),0.5))
                if ipart == 0:
                    if ifreq == 0:
                        axs[ifreq,ipart].set_ylabel('$\\mathrm{Re}(\omega)/\omega_p(0)$',fontsize=fsz)
                    elif ifreq == 1:
                        axs[ifreq,ipart].set_ylabel('$\\mathrm{Im}(\omega)/\omega_p(0)$',fontsize=fsz)
                if ifreq == 0:
                    if ipart == 0:
                        axs[ifreq,ipart].set_title('$\\mathrm{Re}[\widetilde{\epsilon}(q,\omega)]$',size=.8*fsz)
                    elif ipart == 1:
                        axs[ifreq,ipart].set_title('$\\mathrm{Im}[\widetilde{\epsilon}(q,\omega)]$',size=.8*fsz)

                axs[ifreq,ipart].tick_params(axis='both',labelsize=fsz*.8)

        plt.tight_layout()#subplots_adjust(wspace = 0.3,hspace = 0.01)
        fig.colorbar(cplt,ax=axs[0:],extend='both',location='right',shrink=1.0)
        fig.suptitle('$r_s=$'+rs,fontsize=1.2*fsz)
        plt.savefig(base_str+'/dielectric_contours_rs_'+rs+'.pdf',dpi=300,bbox_inches='tight')
        #plt.show()
        #exit()
        plt.cla()
        plt.clf()


    return

def plot_kramers_kronig():
    nbs = base_str+'/../freq_data_new_hx/'
    for rs in [1.0,2.0,3.0,4.0,10.0,20.0,30.0,40.0,50.0,60.0,69.0,100.0]:
        om,fxc_kk,fxc_mod = np.transpose(np.genfromtxt(nbs+'re_fxc_rs_'+str(rs)+'.csv',delimiter=',',skip_header=1))
        fig,ax = plt.subplots(figsize=(10,6))

        ax.plot(om[1:],fxc_kk[1:],color=clist[0])
        ax.plot(om[1:],fxc_mod[1:],color=clist[1])
        #ax.annotate('Kramers-Kronig',(.45,-9.1),color=clist[0],fontsize=16)
        #ax.annotate('Model',(.05,-3.4),color=clist[1],fontsize=16)
        plt.title('Kramers-Kronig (blue) and model (orange), $r_s=$'+str(rs))

        ax.set_xlim([om[1],6.0])
        ax.set_ylabel('$\mathrm{Re}~f_{\mathrm{xc}}(q=0,\omega)$',fontsize=20)
        ax.set_xlabel('$\omega$',fontsize=20)
        ax.tick_params(axis='both',labelsize=16)
        plt.savefig(nbs+'kramers_kronig_comp_rs_'+str(rs)+'.pdf',dpi=600,bbox_inches='tight')

    #plt.show()

    return

def plot_hx():
    new_hx = True

    if new_hx:
        nbs = base_str+'/../test_fits/'
        om,fxc_kk,fxc_mod = np.transpose(np.genfromtxt(nbs+'new_hx.csv',delimiter=',',skip_header=1))
    else:
        nbs = base_str+'/../freq_data/'
        om,fxc_kk,fxc_mod = np.transpose(np.genfromtxt(nbs+'k_k_model_hx_comp.csv',delimiter=',',skip_header=1))
    fig,ax = plt.subplots(figsize=(10,6))

    ax.plot(om,fxc_kk,color=clist[0])
    ax.plot(om,fxc_mod,color=clist[1])
    ax.annotate('Kramers-Kronig',(2,.06),color=clist[0],fontsize=16)
    ax.annotate('Model',(.45,.06),color=clist[1],fontsize=16)

    ax.set_xlim([0.0,6.0])
    ax.set_ylabel('$h(x)$',fontsize=20)
    ax.set_xlabel('$x = b(n)^{1/2}\omega$',fontsize=20)
    ax.tick_params(axis='both',labelsize=16)
    ax.hlines(0.0,plt.xlim()[0],plt.xlim()[1],linestyle='--',color='gray')
    plt.savefig(nbs+'kramers_kronig_model_comp.pdf',dpi=600,bbox_inches='tight')

    #plt.show()

    return


if __name__=="__main__":


    #plots_2d()
    eps_c_plots()
    #spectral_contour_plots()
    #dielectric_contour_plots()
    #plot_kramers_kronig()
    #plot_hx()
