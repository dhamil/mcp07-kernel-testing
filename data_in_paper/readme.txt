NB:

0th, 1st, and 2nd moments generated using:

  + 2,000 pt. Gauss-Legendre grid/interval
  + relative error tolerance 10**-8
  + 0.02 *omega_p(0) intervals
  + Im omega = 10**-10 * omega_p(0)


3rd moment sum rule data generated with:
  + relative error tolerance 10**-7 for both the 3rd frequency moment, and the 2D integral convergence
  + min_cutoff=2
  + cubic spline interpolation of spacing 0.005 k_F
  + same parameters driving the sum rule calculation, besides relative error tolerance


The parameters for the eps_c integration were unchanged from the routines (i.e., the defaults set in get_ec.py)