import numpy as np
from itertools import product
import matplotlib.pyplot as plt
import multiprocessing as mp
from os import path,system

import settings
from mcp07 import chi_mcp07
from gl_grid import gauss_legendre
from qmc_spec import S_QMC
from third_moment_integration import third_moment,set_kc
from moments import int_moments_parser,int_moments
from interpolator import natural_spline
from get_ec import int_eps
from lsda import ec_pz81,eps_x
from kramers_kronig import kramers_kronig_re_fxc

pi = np.pi

rs_l = settings.rs_list # shared by main routines

def get_s(q,rs):
    tmp = int_moments([0.0],save=False,pars={'qlist': np.asarray([q]),'rs':rs,'no_status':True,'min_cutoff':50})[rs]
    return tmp[0,0],tmp[0,1]

def get_chi_data():

    q_l = np.linspace(0.01,3.0,300)
    omega_l = np.linspace(0.01,3.0,300)
    om = np.zeros(omega_l.shape,dtype=complex)

    for rs in rs_l:

        wp = (3.0/rs**3)**(0.5)
        n = 3.0/(4.0*pi*rs**3)
        ofl = open('spectral_rs_'+str(rs)+'.csv','w+')
        ofl2 = open('dielectric_rs_'+str(rs)+'_re_omega.csv','w+')
        ofl3 = open('dielectric_rs_'+str(rs)+'_im_omega.csv','w+')

        ofl.write('q/kf,omega/omega_p(0),S(q,omega)\n')
        ofl2.write('q/kf,Re omega/omega_p(0),Re eps, Im eps\n')
        ofl3.write('q/kf,Im omega/omega_p(0),Re eps, Im eps\n')
        for q in q_l:
            om.imag = 1.0e-5*wp
            om.real = wp*omega_l
            z = 0.5*q
            eps_re,chi = chi_mcp07(z,om,rs,ret_eps=True)
            s = -(chi.imag)/(pi*n)
            om.real = 1.0e-5*wp
            om.imag = wp*omega_l
            eps_im,_ = chi_mcp07(z,om,rs,ret_eps=True,im_omega=True)
            for ianom,anom in enumerate(omega_l):
                ofl.write(('{:},{:},{:}\n').format(q,anom,s[ianom]))
                ofl2.write(('{:},{:},{:},{:}\n').format(q,anom,eps_re[ianom].real,eps_re[ianom].imag))
                ofl3.write(('{:},{:},{:},{:}\n').format(q,anom,eps_im[ianom].real,eps_im[ianom].imag))
        ofl.close()
        ofl2.close()
        ofl3.close()

    return

def spec_comp(do_plot=False,save=False):

    fsz = settings.spec_pars['font_size']

    if do_plot:
        fig,ax = plt.subplots(1,2,figsize=(6,4))

    for irs,rs in enumerate(rs_l):

        dvars = {}
        dvars['rs'] = rs
        dvars['kf'] = (9*pi/4.0)**(1.0/3.0)/rs
        dvars['wp'] = (3.0/rs**3)**(0.5)

        try:
            dat = np.genfromtxt(str(rs)+'_moments.csv',delimiter=',',skip_header=1)
        except IOError:
            raise SystemExit('Missing TD-DFT spectral function data!')

        q_l = dat[:,0]
        s_dft = dat[:,1]

        s_qmc = S_QMC(q_l,dvars)

        bd = [min([s_dft.min(),s_qmc.min()]),max([s_dft.max(),s_qmc.max()])]

        hdr = 'q/kF, S(q) TD-DFT, S(q) QMC'
        np.savetxt(settings.savedir+str(rs)+'_sq_comp.csv',np.transpose((q_l,s_dft,s_qmc)),delimiter=',',header=hdr)

        if do_plot:
            ax[irs].plot(q_l,s_dft,c='tab:blue')
            ax[irs].plot(q_l,s_qmc,c='tab:orange')
            ax[irs].set_xlim([q_l.min(),q_l.max()])
            ax[irs].set_ylim([bd[0],bd[1]])
            ax[irs].set_title('$r_s=$'+str(int(rs)))
            #ax.annotate('TD-DFT',(q_l,)
            ax[irs].set_xlabel('$q/k_F$',fontsize=fsz)
            #plt.cla()
            #plt.clf()
    if do_plot:
        ax[0].set_ylabel('$S(q)$',fontsize=fsz)
        title = 'TD-DFT (blue) and QMC fit (orange) $S(q)$'
        plt.subplots_adjust(hspace = 0.5)
        fig.suptitle(title,fontsize=fsz)
        if save:
            plt.savefig('sq_comp.pdf',dpi=600,bbox_inches='tight')
        else:
            plt.show()

    return


if __name__ == "__main__":

    # check that correct directories have been built
    for dependency in ['grids',settings.savedir+'interp','eps_data','freq_data']:
        if not path.isdir('./'+dependency):
            system('mkdir ./'+dependency)


    if settings.routine == 'moments':
        int_moments_parser([0.0,1.0,2.0])
        #int_moments([0.0,1.0,2.0],save=True)
    elif settings.routine == 'spectral':
        spec_comp(do_plot=settings.spec_pars['plot'],save = settings.spec_pars['save_plot'])
    elif settings.routine == 'contour_data':
        get_chi_data()
    elif settings.routine == 'eps_c':
        ec_l = int_eps(rs_l,settings.epsilon)
        fname = './eps_data/epsilon_'+settings.epsilon+'_'+settings.fxc+'.csv'
        with open(fname,'w+') as ofl:
            ofl.write('rs, eps_'+settings.epsilon+' approx, eps_'+settings.epsilon+' PZ81, % error\n')
            for rs in rs_l:
                if settings.epsilon == 'C':
                    pz = ec_pz81(rs,0.0)
                else:
                    pz = eps_x(rs)
                ofl.write(('{:},{:},{:},{:}\n').format(rs,ec_l[rs],pz,100*(1.0 - ec_l[rs]/pz)))
    elif settings.routine == 'kram_kron':
        for rs in rs_l:
            kramers_kronig_re_fxc(rs)
    elif settings.routine == 'third_moment':

        q_max = settings.int_pars['q_max']
        #interp_grid = './grids/gauss_legendre_'+str(settings.m3_pars['interp_grid'])+'_pts.csv'
        #if not path.isfile(interp_grid) or path.getsize(interp_grid)==0:
        #    gauss_legendre(settings.m3_pars['interp_grid'])

        #_,tmp_l = np.transpose(np.genfromtxt(interp_grid,delimiter=',',skip_header=1))

        opts = {}
        for rs in rs_l:

            flnm = settings.savedir+'interp/Sq_'+str(rs)+'_'+str(settings.m3_pars['interp_grid'])+'spc_tab.csv'
            if not path.isfile(flnm) or path.getsize(flnm)==0:
                if rs in settings.kc_d:
                    kc = settings.kc_d[rs]
                else:
                    if 'kc' not in opts:
                        opts['kc'] = {}
                    tkc,found_kc = set_kc(rs)
                    if found_kc:
                        kc = tkc
                        print(('Determined kc/kF = {:} for rs = {:}').format(kc,rs))
                        opts['kc'][rs]=kc
                    else:
                        raise SystemExit(("Couldn't interpolate S(q) for rs = {:}; no suitable k_c").format(rs))
                k_ubd = ((1.01*q_max)**2 + kc**2 + 2*(1.01*q_max)*kc)**(0.5)
                k_l = np.arange(settings.m3_pars['interp_grid'],k_ubd+settings.m3_pars['interp_grid']*1.001,settings.m3_pars['interp_grid'])
                if settings.ncore > 1:
                    inp_l = [(ak,rs) for ak in k_l]
                    pool = mp.Pool(processes=min(settings.ncore,k_l.shape[0]))
                    t_out = pool.starmap(get_s,inp_l)
                    pool.close()
                    ts =np.asarray( t_out)
                else:
                    ts = int_moments([0.0],save=False,pars={'qlist': k_l,'rs':rs,'no_status':True,'min_cutoff':50})[rs]

                ovars = np.transpose((ts[:,0],ts[:,1]))
                np.savetxt(flnm,ovars,delimiter=',',header='q,S(q)',fmt='%.18f,%.18f')
            else:
                print(('Found tabulated S(q) for rs={:}').format(rs))
                ts=np.genfromtxt(flnm,delimiter=',',skip_header=1)
            opts[rs] = np.transpose((ts[:,0],ts[:,1],natural_spline(ts[:,0],ts[:,1])))

        if settings.ncore > 1:
            qspac = settings.int_pars['n_q_pts']
            ql=np.linspace((1.0*q_max)/(1.0*qspac),q_max,qspac)
            pool = mp.Pool(processes=min(settings.ncore,ql.shape[0]))
            inps = product(ql,[opts])
            t_out = pool.starmap(third_moment,inps)
            pool.close()
            int_d = {}
            sr = {}

            for rs in rs_l:
                int_d[rs] = np.zeros((len(t_out),2))
                sr[rs] = np.zeros(len(t_out))

            for ielt,elt in enumerate(t_out):

                td1,td2 = elt
                for rs in td1:
                    int_d[rs][ielt][0] = td1[rs][0,0]
                    int_d[rs][ielt][1] = td1[rs][0,1]
                    sr[rs][ielt] = td2[rs][0]

            for rs in rs_l:

                cinds = np.argsort(int_d[rs][:,0])
                int_d[rs][:,0] = int_d[rs][:,0][cinds]
                int_d[rs][:,1] = int_d[rs][:,1][cinds]
                sr[rs] = sr[rs][cinds]

                with open(settings.savedir+'rs_'+str(rs)+'_third_moment_sum_rule.csv','w+') as ofl:
                    ofl.write('q/kF, < omega_p(q)**3 >/wp(0)**3, Sum rule/wp(0)**3, < (omega/wp(0))**3*S > - SR\n')
                    for iln,ln in enumerate(int_d[rs]):
                        ofl.write(('{:},{:},{:},{:}\n').format(*ln,sr[rs][iln],ln[1]-sr[rs][iln]))

        else:
            int_d,sr=third_moment('auto',opts)
            for rs in rs_l:
                with open(settings.savedir+'rs_'+str(rs)+'_third_moment_sum_rule.csv','w+') as ofl:
                    ofl.write('q/kF, < omega_p(q)**3 >/wp(0)**3, Sum rule/wp(0)**3, < (omega/wp(0))**3*S > - SR\n')
                    for iln,ln in enumerate(int_d[rs]):
                        ofl.write(('{:},{:},{:},{:}\n').format(*ln,sr[rs][iln],ln[1]-sr[rs][iln]))
    else:
        print(('{:} is not a recognized routine option!').format(settings.routine))
