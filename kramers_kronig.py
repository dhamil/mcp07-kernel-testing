import numpy as np
from scipy.special import gamma
import multiprocessing

import settings
from integrators import nquad
from mcp07 import frequency,exact_constraints

pi = np.pi

def re_fxc_model(rs,omega):
    return frequency(rs,omega.real,x_only=settings.kram_kron_pars['x_only'],new_hx=settings.new_hx).real

def im_fxc_gki(omega):
    # NB: constants only introduced after integration
    return omega/((1.0 + omega**2)**(5.0/4.0))

def wrap_kramers_kronig(to,omega):
    return im_fxc_gki(to)/(to - omega)

def get_kk_re_fxc(om_l):
    if not hasattr(om_l,'__len__'):
        om_l = np.asarray([om_l])
    fxc_re = np.zeros(om_l.shape)

    for iomega,omega in enumerate(om_l):
        fxc_re[iomega],terr = nquad(wrap_kramers_kronig,('-inf','inf'),'global_adap',{'itgr':'GK','prec':1.e-7,'npts':21,'min_recur':20},pars_ops={'PV':[omega]},args=(omega,))
        if terr['code'] == 0:
            print(('WARNING, not converged for omega={:.4f}; last error {:.4e}').format(omega,terr['error']))
    return fxc_re


def kramers_kronig_re_fxc(rs):

    bn,finf = exact_constraints(rs,x_only=settings.kram_kron_pars['x_only'])
    ol = np.linspace(0.0,6.0,1000)

    oscl = ol*bn**(0.5)
    if settings.ncore > 1:
        pool = multiprocessing.Pool(processes=min(settings.ncore,ol.shape[0]))
        refxct = pool.map(get_kk_re_fxc,oscl)
        pool.close()
        refxc = np.zeros(0)
        for tmp in refxct:
            refxc = np.append(refxc,tmp[0])
    else:
        refxc = get_kk_re_fxc(rs,oscl)

    refxc = -(23.0*pi/15.0)*bn**(3.0/4.0)*refxc/pi + finf

    refxc_mod = re_fxc_model(rs,ol)

    if settings.kram_kron_pars['x_only']:
        fname='./freq_data_new_hx/re_fx_rs_'+str(rs)+'.csv'
    else:
        fname='./freq_data_new_hx/re_fxc_rs_'+str(rs)+'.csv'
    np.savetxt(fname,np.transpose((ol,refxc,refxc_mod)),delimiter=',',header='Re omega, K.-K. Re f_xc, Model Re f_xc',fmt='%.18f')
    return

################################################################################

if __name__ == "__main__":

    kramers_kronig_re_fxc(4.0)
