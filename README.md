### Testing the MCP07 XC Kernel
***

### Publication:
J.P. Perdew, A. Ruzsinszky, J. Sun, N.K. Nepal, and A.D. Kaplan, PNAS **118**, e2017850118 (2021), https://doi.org/10.1073/pnas.2017850118
### Data repository:
J. Perdew, A. Ruzsinszky, J. Sun, N. Nepal, A. Kaplan, *Interpretations of ground-state symmetry breaking and strong correlation in wavefunction and density functional theories*, Materials Cloud Archive 2020.173 (2020), https://doi.org/10.24435/materialscloud:vh-wc
### public repository:
https://gitlab.com/dhamil/mcp07-kernel-testing


Adapted from code written by J.P. Perdew, A. Ruzsinszky, J. Sun, and N.K. Nepal

## Maintainer:
A.D. Kaplan

All requisite citations from which the routines have been built are
included in the code. DOIs are included where possible.


## Structure of the Code
***

main.py    :    The main executable, written in python3

               'moments' calculates the 0th, 1st, and 2nd frequency moments of S(q,omega)

               'spectral' compares the MCP07 S(q) with a QMC parameterization

               'third_moment' computes the 3rd frequency moment of S(q,omega), and compares it
                              to a known sum rule

               'contour_data' generates data files of the dielectric function and S(q,omega)
                              suitable for contour plotting

               'eps_c' calculates the correlation energy per electron in jellium. There are
                              auxiliary options available:
                              (a) calculation of the X or XC energies per particle (settings.epsilon)
                              (b) options to use the dynamic or static MCP07 kernel, the RPA, and
                                  PZ81 ALDA (settings.fxc)
               'kram_kron'  calculates the real part of the Gross-Kohn-Iwamoto XC kernel at arbitrary r_s


settings.py    :   All parameters for the routine must be set here prior to executing 'python3 main.py'


## Primary routines:
***

moments.py   :  Calculates arbitrary frequency moments of S(q,omega)

third_moment_integration.py :   The name says it all

get_ec.py   :   Calculates the correlation energy per electron using S(q,omega).

kramers_kronig.py : Calculates the real part of the Gross-Kohn-Iwamoto XC kernel at arbitrary r_s


plotting.py  :  Generates useful plots of the moments of S(q,omega), sum rule comparisons, and contour plots


## Dependencies:
***

qmc_spec.py   :    QMC parameterization of S(q) for jellium

mcp07.py      :   MCP07 dynamic structure factor S(q,omega) for jellium

lsda.py       :   Generates the LSDA exchange energy density and exchange spin potentials,
                  as well as the jellium correlation energy/potentials as parameterized by
                  Perdew and Wang (1992) and Perdew and Zunger (1981).

gauss_quad.py :   Generates Gauss quadrature rules: Legendre, Laguerre, Chebyshev, and Kronrod

gl_grid.py    :   Older version of gauss_quad.py that can only generate Gauss-Legendre quadrature rules

interpolator.py :   Performs cubic spline and Lagrange polynomial interpolation.

integrators.py :   Performs numeric quadrature using a variety of methods. nquad is the primary integrator
                   routine, and allows for both globally-adaptive and single-region-adaptive quadrature.

                   Methods available include:
                   globally-adaptive Gauss-Kronrod quadrature,
                   doubly-adaptive Clenshaw-Curtis quadrature,
                   single-region-adaptive Clenshaw-Curtis quadrature,
                   single-region-adaptive trapezoidal rule,
                   adaptive Tanh-Sinh (double exponential) trapezoidal quadrature

                   As well as generators of the Clenshaw-Curtis mesh and error weights.


## Required Python3 packages:
***

numpy (for efficient calculation of integrals, interpolations, etc.)

itertools (needed to generate multidimensional grids)

matplotlib (needed for plotting)

multiprocessing (needed for multi-core processing, as on HPC)

os (needed to create folder structure and check that all dependencies are written prior to execution)

scipy (needed for various mathematical tools and plotting)

time (in get_ec for testing purposes)
