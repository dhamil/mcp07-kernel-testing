import numpy as np
from itertools import product
import settings
from lsda import ec_pw92,ex_lsda
from gauss_quad import gauss_kronrod
from moments import int_moments
from os import path
from interpolator import spline

pi = np.pi

def set_kc(rs):
    os = -1.0

    conv_par = 1.0e-6#settings.int_pars['diff']
    if rs == 69.0:
        conv_par = 1.0e-3

    kbd_l = np.arange(2.0,10,0.01)
    for tkc in kbd_l:
        if settings.m3_pars['spectral']=='tddft':
            s_tmp = int_moments([0.0],save=False,pars={'qlist': np.asarray([tkc]),'rs':rs,'no_status':True})[rs][:,1][0]
        elif settings.m3_pars['spectral']=='qmc':
            s_tmp = S_QMC(np.asarray([tkc]),dvars)
        if abs(os - s_tmp) < conv_par:
            found_kc = True
            kc = tkc
            break
        else:
            os = s_tmp
    return kc,found_kc

def third_moment(q_l,xopts={}):

    rs_l = settings.rs_list

    no_status = False
    if settings.ncore > 1:
        no_status=True

    pars = settings.m3_pars
    if 'save' not in pars:
        pars['save']=True
    if q_l == 'auto':
        int_d = int_moments([3.0],save=False,pars={'diff': 1.e-6,'min_cutoff': 2,'no_status': no_status})
    else:
        if not hasattr(q_l, '__len__'):
            q_l = [q_l]
        int_d = int_moments([3.0],save=False,pars={'diff': 1.e-7,'min_cutoff': 2,'qlist': q_l,'no_status': no_status})
    sum_rule = {}

    for rs in rs_l:

        int_d[rs] = int_d[rs][:,:2]

        n = 3.0/(4.0*pi*rs**3)
        kf = (3.0*pi**2*n)**(1.0/3.0)
        wp = (3.0/rs**3)**(0.5)
        t0 = 3.0/10.0*kf**2
        z = 0.0 # spin-unpolarized
        if pars['spectral']=='qmc':
            dvars = {}
            dvars['rs'] = rs
            dvars['kf'] = kf
            dvars['wp'] = wp

        ec,vcu,vcd = ec_pw92(rs,0.0)
        tc = -4.0*ec + 1.5*( (1.0 + z)*vcu + (1.0 - z)*vcd )
        #ex,vxu,vxd=ex_lsda(n/2.0,n/2.0)
        t = t0 + tc

        osum = -1.e2
        conv = False

        found_kc = False
        if rs in settings.kc_d:
            kc = settings.kc_d[rs]
            if not no_status:
                print(('Determined kc/kF = {:} from previous run').format(kc))
        else:
            do_search = False
            if 'kc' in xopts:
                if rs in xopts['kc']:
                    kc = xopts['kc'][rs]
                    found_kc = True
                else:
                    do_search = True
            if do_search:
                kc,found_kc = set_kc(rs)
            if found_kc:
                if not no_status:
                    print(('Determined kc/kF = {:}').format(kc))
            else:
                raise SystemExit(("Could not determine kc for rs={:}").format(rs))

        prec = settings.m3_pars['precision']
        n_recur = settings.m3_pars['max_recur'] # recursions per axis

        sum_rule[rs] = np.zeros(int_d[rs][:,0].shape)

        grid_ind_l = [settings.m3_pars['min_grid_size']+2*i for i in range(settings.m3_pars['max_grid_size']+1)]
        grid_l = []
        for agrid in grid_ind_l:
            tgrdnm='./grids/gauss_kronrod_'+str(int(2*agrid+1))+'_pts.csv'
            grid_l.append(tgrdnm)
            if not path.isfile(tgrdnm) or path.getsize(tgrdnm) == 0:
                gauss_kronrod(agrid)

        for iq,q in enumerate(int_d[rs][:,0]):

            tq = kf*q
            sum = 0.0
            not_conv = 0

            need_denser_grid = False
            good_to_go = False

            sum_l = np.zeros(0)#[]
            err_l = np.zeros(0)#[]
            reg_l = np.zeros((0,4))#[]

            for igrid,grid in enumerate(grid_l):

                wg1,u1,u1wg_gl = np.transpose(np.genfromtxt(grid,delimiter=',',skip_header=1))
                wg,u,uwg_gl = np.transpose(np.genfromtxt(grid,delimiter=',',skip_header=1))

                u1,u = np.meshgrid(u1,u)

                if pars['adaptive'] == 'local' and settings.m3_pars['adaptive'] == 'mixed':
                    pars['adaptive'] = 'mixed' # if we had to switch from global to local for a
                    # previous q, switch back


                for ir in range(n_recur+1):#ik,ku in enumerate(kc_l):

                    k_sum = 0.0
                    k_sum_gl = 0.0

                    if ir == 0 and not need_denser_grid:

                        if rs == 69.0:
                            if pars['adaptive'] == 'local':
                                uc_l = [[-1.0,1.0]]
                            elif pars['adaptive'] == 'global' or pars['adaptive'] == 'mixed':
                                uuc_l = np.linspace(-1.0,1.0,pars['min_recur']+1)
                                uc_l = np.zeros((pars['min_recur'],2))
                                for iter in range(len(uuc_l)-1):
                                    uc_l[iter,0] = uuc_l[iter]
                                    uc_l[iter,1] = uuc_l[iter+1]
                            kc_l = [[0.0,2.0],[2.0,2.13],[2.13,2.15],[2.15,2.17],[2.17,2.2],[2.2,kc]]
                        else:
                            if pars['adaptive'] == 'local':
                                uc_l = [[-1.0,1.0]]
                                kc_l = [[0.0,kc]]
                            elif pars['adaptive'] == 'global' or pars['adaptive'] == 'mixed':
                                kc_l = np.zeros((pars['min_recur'],2))
                                uc_l = np.zeros((pars['min_recur'],2))
                                uuc_l = np.linspace(-1.0,1.0,pars['min_recur']+1)
                                kkc_l = np.linspace(0.0,kc,pars['min_recur']+1)
                                for iter in range(len(uuc_l)-1):
                                    uc_l[iter,0] = uuc_l[iter]
                                    uc_l[iter,1] = uuc_l[iter+1]
                                    kc_l[iter,0] = kkc_l[iter]
                                    kc_l[iter,1] = kkc_l[iter+1]
                    elif ir > 0:
                        kc_l = np.asarray(tc_l)
                        uc_l = np.asarray(tu_l)

                    tc_l = []
                    tu_l = []
                    #if pars['adaptive'] == 'global' or pars['adaptive'] == 'mixed':
                    #    sum_l = []
                    #    err_l = []
                    #    reg_l = []

                    for tens in product(uc_l,kc_l):

                        uul,kul = tens

                        ulbd,uubd = uul
                        u_g = 0.5*(uubd-ulbd)*u + 0.5*(uubd+ulbd)
                        uwg = 0.5*(uubd-ulbd)*wg
                        twg_gl= 0.5*(uubd-ulbd)*uwg_gl

                        lbd,ku = kul
                        k_g = 0.5*(ku-lbd)*u1 + 0.5*(ku+lbd)
                        wgk = 0.5*(ku-lbd)*kf*wg1
                        wgk_gl = 0.5*(ku-lbd)*kf*u1wg_gl

                        qmk = (q**2 + k_g**2 - 2*q*k_g*u_g)**(0.5)

                        s_qmk = np.zeros(qmk.shape)
                        s_k_g = np.zeros(qmk.shape)
                        for irow,row in enumerate(qmk):
                            if pars['spectral']=='tddft':
                                if settings.m3_pars['use_interp']:
                                    s_qmk[irow] = spline(row,xopts[rs][:,0],xopts[rs][:,1],xopts[rs][:,2])
                                else:
                                    s_qmk[irow] = int_moments([0.0],save=False,pars={'qlist': row,'rs':rs,'no_status':True})[rs][:,1]
                            elif pars['spectral']=='qmc':
                                s_qmk[irow] = S_QMC(row,dvars)
                            if irow == 0:
                                if pars['spectral']=='tddft':
                                    if settings.m3_pars['use_interp']:
                                        s_k_g[irow] = spline(k_g[irow],xopts[rs][:,0],xopts[rs][:,1],xopts[rs][:,2])
                                    else:
                                        s_k_g[irow] = int_moments([0.0],save=False,pars={'qlist': k_g[irow],'rs':rs,'no_status':True})[rs][:,1]
                                elif pars['spectral']=='qmc':
                                    s_k_g[irow] = S_QMC(k_g[irow],dvars)
                            else:
                                s_k_g[irow] = s_k_g[0]

                        integrand = (kf*k_g*u_g)**2*(s_qmk-s_k_g)

                        k_sum = 1.0/pi*np.sum(wgk*np.matmul(uwg,integrand))
                        k_sum_gl = 1.0/pi*np.sum(wgk_gl*np.matmul(twg_gl,integrand))
                        err = abs(k_sum_gl-k_sum) # well-known error from Gauss-Kronrod integration
                        rat = k_sum/k_sum_gl

                        if pars['adaptive'] == 'local': # bisection of each region where integration failed
                            conv = False
                            if err < prec:
                                conv = True
                            if err/abs(k_sum_gl) > 0.5 or err/abs(k_sum) > 0.5 or rat < 0.0:
                                conv = False
                            if abs(rat) > 1.5 or 1.0/abs(rat) > 1.5:
                                conv = False
                            #print(ir,k_sum,k_sum_gl,conv)

                            if conv: # did we converge?
                                sum += k_sum
                                continue
                            elif not conv:# and ir < n_recur: # adaptive recursions
                                mid = 0.5*(ku+lbd) # bisect region where convergence failed
                                tc_l.append([lbd,mid]) # only repeat integrations over regions
                                tc_l.append([mid,ku]) # for which we couldn't reach desired precision
                                mid = 0.5*(uubd+ulbd)
                                tu_l.append([ulbd,mid])
                                tu_l.append([mid,uubd])
                            if not conv and ir == n_recur and igrid < len(grid_l)-1:
                                need_denser_grid = True # first try denser grid
                            elif not conv and ir == n_recur and igrid == len(grid_l)-1:
                                sum += k_sum # well we tried our best, but we couldn't do it
                                not_conv+=1 # we still need to estimate the integral, but
                                # we'll print a warning letting the user know that integration failed
                        elif pars['adaptive'] == 'global' or pars['adaptive'] == 'mixed':
                            # global: bisection of region only with largest error
                            sum_l = np.append(sum_l,k_sum) # to do this, we need to store the info about
                            err_l = np.append(err_l,err) # each region
                            reg_l = np.vstack((reg_l,[uul[0],uul[1],kul[0],kul[1]]))

                    if pars['adaptive'] == 'global' or pars['adaptive'] == 'mixed': # now we can partition regions
                        if np.sum(err_l) < prec:
                            sum += np.sum(sum_l)
                        else:
                            if pars['adaptive'] == 'mixed':
                                bad_reg_ind = err_l>=prec
                                n_fail = len(err_l[bad_reg_ind])
                            #if ir < n_recur: # if there are still further steps left,
                            if pars['adaptive'] == 'mixed' and n_fail > int(np.ceil(len(err_l)/2.0)):
                                # if too many regions failed, switch to local
                                pars['adaptive'] = 'local'
                                sum += np.sum(sum_l[err_l<prec])
                                for reg in reg_l[bad_reg_ind]:
                                    tu,tk = reg
                                    tc_l.append([tk[0],0.5*(tk[0]+tk[1])])
                                    tc_l.append([0.5*(tk[0]+tk[1]),tk[1]])
                                    tu_l.append([tu[0],0.5*(tu[0]+tu[1])])
                                    tu_l.append([0.5*(tu[0]+tu[1]),tu[1]])
                            else:
                                #bad_reg = np.argmax(err_l) # partition region with largest error
                                #sum += np.sum(sum_l)-sum_l[bad_reg]
                                sorted_inds = np.argsort(err_l)
                                reg_l = reg_l[sorted_inds]# partition region with largest error
                                tu = reg_l[-1][:2]
                                tk = reg_l[-1][2:]
                                reg_l = reg_l[:-1] # remove worst element from current list
                                err_l = err_l[sorted_inds][:-1]
                                sum_l = sum_l[sorted_inds][:-1]
                                tu_l.append([tu[0],0.5*(tu[0]+tu[1])])
                                tu_l.append([0.5*(tu[0]+tu[1]),tu[1]])
                                tc_l.append([tk[0],0.5*(tk[0]+tk[1])])
                                tc_l.append([0.5*(tk[0]+tk[1]),tk[1]])
                            if ir == n_recur and igrid<len(grid_l)-1: # but if we're out of iterations, and we still failed,
                                need_denser_grid = True#try to bump up the grid size first, for all badly-behaved regions
                            elif ir == n_recur and igrid == len(grid_l)-1:
                                #sum += np.sum(sum_l) # move on and print an error message
                                not_conv += 1

                    if not_conv > 0 and ir == n_recur and igrid == len(grid_l)-1:
                        print(('WARNING: for rs = {:}, q/kF = {:.2f},').format(rs,q))
                        print(('failed to converge within {:} precision in {:} recursive bisections').format(prec,n_recur**2))
                        print(('Last error {:.4e}').format(np.sum(err_l)))
                    if len(tc_l)==0 and len(tu_l) == 0: # if all integrals are reasonably converged, move on to the next q
                        good_to_go = True
                        break
                if good_to_go:
                    break
            if pars['adaptive'] == 'global' or pars['adaptive'] == 'mixed':
                sum = np.sum(sum_l) # regardless of convergence, give an estimate of the integral

            sum_rule[rs][iq] = tq**2/2.0*(tq**4/4.0 + 4*pi*n + 2*tq**2*t + sum)/wp**3#/sss[rs][iq]
            #print(int_d[rs][iq],sum_rule[rs][iq])

        if pars['save']:
            with open('rs_'+str(rs)+'_third_moment_sum_rule.csv','w+') as ofl:
                ofl.write('q/kF, < omega_p(q)**3 >/wp(0)**3, Sum rule/wp(0)**3, < (omega/wp(0))**3*S > - SR\n')
                for iln,ln in enumerate(int_d[rs]):
                    ofl.write(('{:},{:},{:},{:}\n').format(*ln,sum_rule[rs][iln],ln[1]-sum_rule[rs][iln]))

    return int_d,sum_rule
