import numpy as np

def gauss_legendre(lvl):

    lvl = int(np.ceil(lvl))

    fname = './grids/gauss_legendre_{:}_pts.csv'.format(lvl)

     # algorithm from Golub and Welsch, Math. Comp. 23, 221 (1969)
    def bet(n):# coefficients from NIST's DLMF, sec. 18.9
        an = (2*n+1.0)/(n+1.0)
        anp1 = (2*n+3.0)/(n+2.0)
        c = (n+1.0)/(n+2.0)
        return (c/an/anp1)**(0.5)
    jac = np.zeros((lvl,lvl))
    jac[0,1] = bet(0)
    jac[lvl-1,lvl-2] = bet(lvl-2)
    for jn in range(1,lvl-1):
        jac[jn,jn+1] = bet(jn)
        jac[jn,jn-1] = bet(jn-1)
    grid,v = np.linalg.eigh(jac)
    wg = 2*v[0]**2

    np.savetxt(fname,np.transpose((wg,grid)),delimiter=',',fmt='%.16f',header='weight,point')

    return

if __name__=="__main__":

    gauss_legendre(5e1)
