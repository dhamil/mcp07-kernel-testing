import numpy as np
from mcp07 import chi_mcp07
import multiprocessing as mp
from itertools import product

import settings

pi = np.pi

def moment(x,y,dx,m):
    return np.sum(x**m*y*dx)

def int_moments(moment_l,save=True,pars={}):

    ret_d = {}

    for flag in ['diff','q_max','n_q_pts','min_cutoff','max_cutoff']:
        if flag not in pars:
            pars[flag] = settings.int_pars[flag]

    if settings.int_pars['grid_type'] == 'GLEG':
        grid = './grids/gauss_legendre_'+str(settings.int_pars['grid_pts'])+'_pts.csv'
        try:
            wg,u = np.transpose(np.genfromtxt(grid,delimiter=',',skip_header=1))

        except IOError:
            gauss_legendre(settings.int_pars['grid_pts'])
            wg,u = np.transpose(np.genfromtxt(grid,delimiter=',',skip_header=1))

        omega = np.ones(u.shape,dtype=complex)
        twg = np.ones(wg.shape)

    diff = pars['diff']
    q_max = pars['q_max']
    qspac = pars['n_q_pts']
    icut_min = pars['min_cutoff']
    icut_max = pars['max_cutoff']

    if 'qlist' in pars:# list of q/kF values
        q_l = pars['qlist']
    else:
        q_l = np.linspace((1.0*q_max)/(1.0*qspac),q_max,qspac)
    icut_l = np.arange(0.02,icut_max+.01,0.02) # list of upper integration bounds

    if 'rs' in pars:
        trs_l = [pars['rs']]
    else:
        trs_l = settings.rs_list
    if 'no_status' not in pars:
        pars['no_status']=False

    for rs in trs_l: # cycling through list of rs values

        int_l = np.zeros((len(q_l),7)) # storing outputs
        w_l = np.zeros(len(q_l),dtype=int)

        n = 3.0/(4.0*pi*rs**3)
        kf = (3.0*pi**2*n)**(1.0/3.0)
        wp = (3.0/rs**3)**(0.5)

        #dvars = {'rs':rs,'n':n,'kF':kf,'wp':wp}

        if settings.int_pars['grid_type'] == 'GLEG':
            omega.imag = 1.e-10*wp

        nconv = 0

        for iq,q in enumerate(q_l): # cycle through q/kF values

            z = 0.5*q
            int_l[iq,0] = q

            for imom,mom in enumerate(moment_l): # cycle through moments

                osum = -1.0e2
                conv = False
                tsum = 0.0

                for it,icut in enumerate(icut_l): # integration upper bound

                    if settings.int_pars['grid_type'] == 'trapezoid':
                        step = settings.int_pars['step']
                        if it == 0:
                            u = wp*np.arange(step,icut+step,step)
                        else:
                            u = wp*np.arange(icut_l[it-1],icut+step,step)
                        omega = np.zeros(u.shape,dtype=complex)
                        omega.real = u
                        omega.imag = 1.e-10*wp
                        twg = wp*step*np.ones(u.shape)
                        twg[0] *= 0.5
                        twg[-1] *= 0.5
                    elif settings.int_pars['grid_type'] == 'GLEG':
                        if it == 0:
                            lesser = 0.0
                        else:
                            lesser = icut_l[it-1]
                        omega.real = 0.5*wp*(icut-lesser)*u + 0.5*wp*(icut+lesser)
                        twg = 0.5*wp*(icut-lesser)*wg

                    chi = chi_mcp07(z,omega,rs,new_hx=settings.new_hx)
                    s = -(chi.imag)/(pi*n)

                    if settings.int_pars['grid_type'] == 'trapezoid':
                        tsum += moment((omega.real)/wp,s,twg,mom)
                    elif settings.int_pars['grid_type'] == 'GLEG':
                        tsum += moment((omega.real)/wp,s,twg,mom)

                    if abs(osum - tsum) < diff*abs(osum) and icut >= icut_min+q:
                        nconv+=1
                        conv = True
                        break
                    else:
                        osum = tsum

                if conv:
                    int_l[iq,1 + 2*imom] = tsum
                    int_l[iq,2 + 2*imom] = icut
                else:
                    w_l[iq]=1

        if nconv < len(moment_l)*len(q_l):
            print(('Warning, {:} integrals out of {:} total not converged (rs={:})').format(len(moment_l)*len(q_l)-nconv,len(moment_l)*len(q_l),rs))

        if save:
            hdr = 'q/kF,<1>,cut/omega_p(0),<omega_p(q)>/omega_p(0),cut/omega_p(0),<omega_p(q)**2/omega_p(0)**2>,cut/omega_p(0)'
            with open(str(rs)+'_moments.csv','w+') as ofl:
                ofl.write(hdr+'\n')
                for iq in range(len(q_l)):
                    if w_l[iq] != 1:
                        ofl.write(('{:},'*6 + '{:}\n').format(*int_l[iq]))
        else:
            ret_d[rs] = []
            for iln,ln in enumerate(int_l):
                if w_l[iln] != 1:
                    ret_d[rs].append(ln)
            ret_d[rs] = np.asarray(ret_d[rs])
    return ret_d

def int_moments_multi_wrapper(moment_l,q):

    return int_moments(moment_l,save=False,pars={'qlist':np.asarray([q])})

def int_moments_parser(moment_l):

    if settings.ncore > 1:
        q_max = settings.int_pars['q_max']
        qspac = settings.int_pars['n_q_pts']
        q_l = np.linspace((1.0*q_max)/(1.0*qspac),q_max,qspac)
        pool = mp.Pool(processes=min(settings.ncore,q_l.shape[0]))
        inps = product([moment_l],q_l)
        t_out = pool.starmap(int_moments_multi_wrapper,inps)
        pool.close()
        int_d = {}
        for rs in settings.rs_list:
            int_d[rs] = []
        for row in t_out:
            for an_rs in row:
                int_d[an_rs].append(row[an_rs][0])
        for rs in int_d:
            hdr = 'q/kF,<1>,cut/omega_p(0),<omega_p(q)>/omega_p(0),cut/omega_p(0),<omega_p(q)**2/omega_p(0)**2>,cut/omega_p(0)'
            with open(str(rs)+'_moments.csv','w+') as ofl:
                ofl.write(hdr+'\n')
                for iq in range(len(int_d[rs])):
                        ofl.write(('{:},'*6 + '{:}\n').format(*int_d[rs][iq]))
    else:
        int_moments(moment_l,save=True)
