import numpy as np
from scipy.special import factorial

pi = np.pi

def S_QMC(k,dvars): # NB: k = q/kF, q is wavevector

    rs = dvars['rs']
    kf = dvars['kf']
    wp = dvars['wp']

    # adapted from
    # Paola Gori-Giorgi, Francesco Sacchetti, and Giovanni B. Bachelet
    # Phys. Rev. B 61, 7353 (2000)
    # doi: 10.1103/PhysRevB.61.7353

    auu = 1.32
    aud = 0.838
    alpha = (9*pi/4.0)**(1.0/3.0) # factor to go from rs to kF

    qmc_pars = { # lambda and gamma parameters in Eqs. 51 & 52
    'lambda': {'uu': {4: 98.0, 5: -295.0, 6: 170.0},
      'ud': {4: -78.0, 5: 216.0, 6: -140.0} },
    'gamma': {'uu': {4: -36.0, 5: 74.0, 6: -13.0},
      'ud': {4: 28.0, 5: -124.0, 6: 55.0} },
    'b1': {'uu': 3.47, 'ud': 3.27 }
    }

    Ah = (1.0 - np.log(2.0))/(2*pi**2) # Auu = Aud = A/2

    B = -0.0469205
    zet3 = 1.20205690315959428539973816151144999076 # OEIS, A002117, Riemann zeta of 3, or Apery's const.
    Bexc = 0.5*(np.log(2.0)/3.0-1.5/pi**2*zet3) # Gary G. Hoffman, Phys. Rev. B 45, 8730 (1992).
    Bd = B - Bexc
    Bupdn = Bd / 2.0 # Look between Eq 35 and 36, and between Eq. 46 and 47.
    Bupup = Bexc + Bupdn

    p1 = 33*pi*Ah*auu**4 / alpha   #Eq 54
    p2 = np.exp(7.0/(960*pi*auu**5*Ah) - 81.0/(128*pi*alpha**3*Ah) - Bupup/Ah - 0.5) # Eq 55
    p3 = 0.015

    k1 = 18*pi*aud**2*Ah/alpha  #eq 39
    k2t = np.exp(7.0/(384*pi*aud**3*Ah) - 81.0/(128*pi*alpha**3*Ah) - Bupdn/Ah - 0.5) # eq 53
    k3 = 0.141

    a4ud = -4.0*(1.0 - k1*rs*np.log(1.0 + k2t/rs) ) / (3*pi*kf*(1.0 + k3*rs**2))  #eq 48
    a6uu = 8.0*(1.0 - p1*rs*np.log(1.0 + p2/rs) ) / (5.0*pi*kf*(1.0 + p3*rs**2)) # Eq 49

    def B(spin): # Eq 51
        b = pi/alpha*(3.0/rs)**(0.5) + qmc_pars['b1'][spin]
        return b

    def C(n,spin): # Eq 52
        L = qmc_pars['lambda'][spin][n]
        g = qmc_pars['gamma'][spin][n]
        c = (L + g*rs)/(1.0 + rs**(1.5))
        return c

    b = {'uu': B('uu'), 'ud': B('ud') } # only need to calculate these once

    cn = {'uu': np.zeros(6),'ud': np.zeros(6)}
    cn['uu'][0] = -3.0/8.0
    cn['ud'][0] = -3.0/8.0
    for s in ['uu','ud']:
        cn[s][1] = b[s]*cn[s][0] + kf**2/(4*wp)
        cn[s][2] = b[s]**2 *cn[s][0]/2.0 + b[s]*kf**2/(4*wp) + 1.0/32.0
        for i in range(4,7):
            cn[s][i-1] = C(i,s)

    k_pow = np.zeros((6,len(k)))
    for i in range(1,7):
        k_pow[i-1] = k**i

    def Alpha6updn(): # Eq 33

        a6ud = a4ud*(-11.0/aud - 512.0*kf/21.0)
        fac_l = np.asarray([factorial(n+2)/b['ud']**(n+3) for n in range(1,7)])
        a6ud -= 2048.0*(1.0/3.0 + np.dot(cn['ud'],fac_l))/(21.0*pi)

        return a6ud*aud**3

    def Alpha8upup(): # Eq 44

        fac_l = np.asarray([(factorial(n+2) - 5.0*factorial(n+4)/(11.0*(auu*b['uu'])**2))/b['uu']**(n+3)  for n in range(1,7)])

        tot = np.dot(cn['uu'],fac_l)
        alpha8 = 2048.0/(3.0*pi)*auu**5*tot
        alpha8 += 4096.0*auu**3/(33.0*pi)
        alpha8 -= a6uu*auu**3*(2560.0*kf/33.0 + 26.0/auu)
        return alpha8

    def Alpha10upup(): # Eq 45

        fac_l = np.asarray([( factorial(n+4)/(auu*b['uu'])**2 - 13.0*factorial(n+2)/3.0) /b['uu']**(n+3) for n in range(1,7)])
        tot = np.dot(cn['uu'],fac_l)
        alpha10 = 2048.0/(3*pi)*auu**7*tot
        alpha10 -= 4096.0*auu**5/(15*pi)
        alpha10 += a6uu*auu**5*( 143.0/auu + 512.0*kf )/3.0
        return alpha10

    def Scupup(): #Eq 41

        a10uu = Alpha10upup()
        a8uu = Alpha8upup()
        scupup = np.exp(-b['uu']*k)*np.dot(cn['uu'],k_pow)
        scupup += (a10uu*k**8 + a8uu*k**10 + a6uu*k**12)/(auu**2 + k**2)**9
        return scupup

    def Scupdn(): # Eq 27

        a6ud = Alpha6updn()
        scupdn = np.exp(-b['ud']*k)*np.dot(cn['ud'],k_pow)
        scupdn += (a6ud*k**8 + a4ud*k**10)/(aud**2 + k**2)**7
        return scupdn

    # Eq. 26
    sx = np.ones(k.shape)
    sx[k<=2.0] = 3.0*k[k<=2.0]/4.0 - k[k<=2.0]**3/16.0 # exchange

    return sx + Scupup() + Scupdn() # last two terms are correlation


if __name__ == "__main__":
    dvars = {}
    dvars['rs'] = 4.0
    dvars['kf'] = (9*pi/4.0)**(1.0/3.0)/dvars['rs']
    dvars['wp'] = (3.0/dvars['rs'])**(0.5)

    print(S_QMC(1.0,dvars))
