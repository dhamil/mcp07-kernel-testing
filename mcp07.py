import numpy as np
from scipy.special import gamma

from integrators import nquad
import settings

pi = np.pi

# universal constants in this module

gam = (gamma(0.25))**2/(32*pi)**(0.5)#(3.6256)**2/(32*pi)**(0.5)
cc = 23.0*pi/15.0

# From J. P. Perdew and Alex Zunger,
# Phys. Rev. B 23, 5048, 1981
# doi: 10.1103/PhysRevB.23.5048
# for rs < 1
au = 0.0311
bu = -0.048
cu = 0.0020
du = -0.0116

# for rs > 1
gu = -0.1423
b1u = 1.0529
b2u = 0.3334
gp = -0.0843
b1p = 1.3981
b2p = 0.2611

b = (3.0/(4.0*pi))**(1.0/3.0)

def chi_mcp07(z,omega,rs,ret_eps=False,reduce_omega=False,im_omega=False,ixn=1.0,wfxc='MCP07',new_hx=False):

    kf = (9.0*pi/4.0)**(1.0/3.0)/rs
    ef = kf**2/2.0
    q = 2*kf*z
    if reduce_omega:
        ufreq = omega/(4*z)
    else:
        ufreq = omega/(4*z*ef)
    chi0 = -kf/pi**2*lindhard(z,ufreq)
    vc = 4*pi*ixn/q**2

    if 0.0 == ixn: # noninteracting response function is just Lindhard/KS response
        return chi0
    elif 0.0 < ixn: # if interaction strength is positive, appropriately scale
        rs *= ixn # Hartree and XC kernels
        q /= ixn
        omega /= ixn**2
    if reduce_omega:
        omega *=ef

    if wfxc == 'MCP07':
        fxc_q,f0,akn = mcp07(z,rs)
        if im_omega:
            fxc_omega = frequency(rs,omega,axis='imag',new_hx=new_hx,use_par=settings.im_omega_param)
        else:
            fxc_omega = frequency(rs,omega.real,axis='real',new_hx=new_hx)
        fxc = (1.0 + np.exp(-akn*q**2)*(fxc_omega/f0 - 1.0))*fxc_q/ixn
    elif  wfxc == 'MCP07_static':
        fxc_q,f0,akn = mcp07(z,rs)
        fxc = fxc_q/ixn
    elif wfxc == 'MCP07_inf':
        fxc_q,f0,akn = mcp07(z,rs)
        _,finf = exact_constraints(rs)
        fxc = (1.0 + np.exp(-akn*q**2)*(finf/f0 - 1.0))*fxc_q/ixn
    elif wfxc == 'MCP07_undamp':
        fxc_q,f0,akn = mcp07(z,rs)
        if im_omega:
            fxc_omega = frequency(rs,omega,axis='imag',new_hx=new_hx,use_par=settings.im_omega_param)
        else:
            fxc_omega = frequency(rs,omega.real,axis='real',new_hx=new_hx)
        fxc = fxc_omega/f0*fxc_q/ixn
    elif wfxc == 'ALDA':
        fxc = alda(rs)/ixn
    elif wfxc == 'RPA':
        fxc = 0.0
    else:
        raise SystemExit('WARNING, unrecognized XC kernel',wfxc)

    eps = 1.0 - (vc + fxc)*chi0
    if ret_eps: # True to return only the dynamic dielectric function
        return eps, chi0/eps
    else:
        return chi0/eps

def lindhard(z,uu):

    zu1 = z - uu + 0.0j
    zu2 = z + uu + 0.0j

    fx = 0.5 + 0.0j
    fx += (1.0-zu1**2)/(8.0*z)*np.log((zu1 + 1.0)/(zu1 - 1.0))
    fx += (1.0-zu2**2)/(8.0*z)*np.log((zu2 + 1.0)/(zu2 - 1.0))
    return fx

def alda(rs,x_only=False):

    n = 3.0/(4.0*pi*rs**3)
    kf = (3.0*pi**2*n)**(1.0/3.0)
    rsh = rs**(0.5)

    # The uniform electron gas correlation energy according to
    # Perdew and Zunger, Phys. Rev. B, 23, 5076 (1981)

    if rs < 1.0:

        ALDAx = -1.0/(3**2*pi*n**2)**(1.0/3.0)
        ALDAc = -6.0**(1.0/3.0)*(cu+2*du)-6.0*au*(pi*n)**(1.0/3.0)
        ALDAc -= 2.0*6.0**(1.0/3.0)*cu*np.log(rs)
        ALDAc /= (18.0*(pi*n**4)**(1.0/3.0))
        if x_only:
            ALDAxc = ALDAx
        else:
            ALDAxc = ALDAx + ALDAc

    else:

        akc = kf**2/(4.0*pi)

        fac1 = b*b2u*(21.0*b1u +16.0*b2u*rsh)
        fac2 = 5.0*b1u + 7.0*(b1u**2)*rsh + 8.0*b2u*rsh
        decdrsn = gu*b**2*(fac1 + fac2*b/rs)
        decdrsd = 36.0*(b*b1u+(b*b2u+b/rs)*rsh)**3*(b/rs)**3
        decdrs = decdrsn/decdrsd
        if x_only:
            ALDAxc = - 1.0/(4.0*akc)
        else:
            ALDAxc = decdrs - 1.0/(4.0*akc)

    return ALDAxc

def mcp07(z,rs):

    n = 3.0/(4.0*pi*rs**3)
    kf = (3.0*pi**2*n)**(1.0/3.0)
    q = 2*kf*z
    rsh = rs**(0.5)
    cfac = 4*pi/kf**2

    # bn according to the parametrization of Eq. (7) of
    # Massimiliano Corradini, Rodolfo Del Sole, Giovanni Onida, and Maurizia Palummo
    # Phys. Rev. B 57, 14569 (1998)
    # doi: 10.1103/PhysRevB.57.14569
    bn = 1.0 + 2.15*rsh + 0.435*rsh**3
    bn /= 3.0 + 1.57*rsh + 0.409*rsh**3
    f0 = alda(rs)
    akn = -f0/(4.0*pi*bn)

    # The rs-dependent cn

    cn = -pi/(2.0*kf)
    if rs >= 1.0:

        rsuff = 0.5*b1u/rsh + b2u
        rsfu = gu/(1.0 + b1u*rsh + b2u*rs)
        rssu = (1.0 + b1u*rsh + b2u*rs)**2

        cn *= rsfu - gu*rs*rsuff/rssu
    else:

        cn *= au + bu + au*np.log(rs) + 2.0*cu*rs*np.log(rs) + cu*rs + 2.0*du*rs

    # The gradient term
    cxcn = 1.0 + 3.138*rs + 0.3*rs**2
    cxcd = 1.0 + 3.0*rs + 0.5334*rs**2
    cxc = -0.00238 + 0.00423*cxcn/cxcd
    dd = 2.0*cxc/(n**(4.0/3.0)*(4.0*pi*bn)) - 0.5*akn**2

    # The MCP07 kernel
    vc = 4.0*pi/q**2
    cl = vc*bn
    zp = akn*q**2
    grad = 1.0 + dd*q**4
    cutdown = 1.0 + 1.0/(akn*q**2)**2
    fxcmcp07 = cl*(np.exp(-zp)*grad - 1.0) - cfac*cn/cutdown

    return fxcmcp07,f0,akn

def exact_constraints(rs,x_only=False):

    ctil = -3.0/(4*pi)*(3*pi**2)**(1.0/3.0)

    n = 3.0/(4*pi*rs**3)
    kf = (3*pi**2*n)**(1.0/3.0)

    f0 = alda(rs,x_only=x_only)

    if rs >= 1.0:

        oon3 = n**(-1.0/3.0)

        ffac1 = -2.0/3.0*oon3**5*gu
        df = 1.0 + b1u*b**(0.5)/n**(1.0/6.0)
        df += b2u*b*oon3
        d1 = 1.0/df

        dd1 = -b1u*b**(0.5)/n**(7.0/6.0)/6.0
        dd1 += -b2u*b*oon3**4/3.0

        ffac2 = gu*oon3**2
        dd2 = df**2

        ft = ffac1*d1 - ffac2*dd1/dd2
        ffac3 = -(1.0/3.0)*gu*oon3**4
        ffac4 = gu*oon3

        st = ffac3*d1 - ffac4*dd1/dd2
        fxch1 = -(4.0/5.0)*n**(2.0/3.0)*ft
        fxch2 = 6.0*n**(1.0/3.0)*st
        fxh = (4.0*ctil/15.0)*oon3**2

        if x_only:
            finf = fxh
        else:
            finf = fxh + fxch1 + fxch2

    elif rs < 1.0:

        tx1 = (3.0/(pi*n**4))**(1.0/3.0)/4.0
        np3 = (pi*n)**(1.0/3.0)
        s3 = 6.0**(1.0/3.0)

        ala = np.log(rs)
        alb = -3*np.log(rs)

        tc1 = -s3*(cu + 3*du) - 2*(au + 2*bu)*np3
        tc1 -= 4*au*np3*ala + s3*cu*alb
        tc1 /= 6*n**2*pi**(1.0/3.0)

        tc2 = -s3*cu - 2.0*( s3*du + (au+bu)*np3 )
        tc2 -= 2.0*(s3*cu + au*np3)*ala
        tc2 /= 6.0*np3*n**(4.0/3.0)
        if x_only:
            finf = -(4.0/5.0)*n**(2.0/3.0)*tx1
        else:
            finf = -(4.0/5.0)*n**(2.0/3.0)*(tx1 + tc1) + 6.0*n**(1.0/3.0)*tc2

    bfac = (gam/cc)**(4.0/3.0)
    deltaf = finf - f0
    if deltaf <= 1.e-9:
        deltaf = 1.e-9

    bn = bfac*deltaf**(4.0/3.0)

    return bn,finf

def frequency_real(rs,u,x_only=False,new_hx=False,dimensionless=False):
    if dimensionless:
        xk = u
    else:
        # The exact constraints are the low and high-frequency limits
        bn,finf = exact_constraints(rs,x_only=x_only)
        bnh = bn**(0.5)

        xk = bnh*u
    gx = xk/((1.0 + xk**2)**(5.0/4.0))
    if new_hx:
        apar = 1.0067
        bpar = 0.7500
        cpar = 0.7746
        hx = 1.0/gam*(1.0 + bpar*xk**cpar - apar*xk)
        hx /= (1.0  + (apar/gam)**(4.0/5.0)*xk**2)**(5.0/4.0)
    else:
        aj = 0.63
        h0 = 1.0/gam#1.311
        hx = h0*(1.0 - aj*xk**2)
        hx /= (1.0 + (h0*aj)**(4.0/7.0)*xk**2)**(7.0/4.0)

    fxcu = np.ones(u.shape,dtype=complex)
    if dimensionless:
        fxcu.real = hx
        fxcu.imag = gx
    else:
        fxcu.real = finf - cc*bn**(3.0/4.0)*hx
        fxcu.imag = -cc*bn**(3.0/4.0)*gx

    return fxcu

def frequency(rs,u,axis='real',x_only=False,new_hx=False,use_par=False):

    if not hasattr(u,'__len__'):
        u = u*np.ones(1)
    if axis == 'real':
        fxcu = frequency_real(rs,u,x_only=x_only,new_hx=new_hx)
    elif axis == 'imag':
        fxcu = np.zeros(u.shape)
        bn,finf = exact_constraints(rs,x_only=x_only)
        if use_par:
            if not new_hx and not x_only:
                cpars = [1.06971,1.52708]#[1.06971136,1.52708142] # higher precision values destabilize the integration
                interp = 1.0/gam/(1.0 + (u.imag*bn**(0.5))**cpars[0])**cpars[1]
            fxcu = -cc*bn**(3.0/4.0)*interp + finf
        else:
            def wrap_integrand(tt,freq,rescale=False):
                if rescale:
                    alp = 0.1
                    to = 2*alp/(tt+1.0)-alp
                    d_to_d_tt = 2*alp/(tt+1.0)**2
                else:
                    to = tt
                    d_to_d_tt = 1.0
                tfxc = frequency_real(rs,to,x_only=x_only,new_hx=new_hx,dimensionless=True)
                num = freq*tfxc.real + to*tfxc.imag
                denom = to**2 + freq**2
                return num/denom*d_to_d_tt
            for itu,tu in enumerate(u):
                rf = tu.imag*bn**(0.5)
                fxcu[itu],err = nquad(wrap_integrand,(-1.0,1.0),'global_adap',{'itgr':'GK','npts':5,'prec':1.e-8},args=(rf,),kwargs={'rescale':True})
                if err['error'] != err['error']:
                    fxcu[itu],err = nquad(wrap_integrand,(0.0,'inf'),'global_adap',{'itgr':'GK','npts':5,'prec':1.e-8},args=(rf,))
                if err['code'] == 0:
                    print(('WARNING, analytic continuation failed; error {:}').format(err['error']))
                fxcu[itu] = -cc*bn**(3.0/4.0)*fxcu[itu]/pi + finf
    return fxcu


if __name__ == "__main__":

    #rs = 4
    """
    w = np.linspace(0.0001,10.01,1000)
    import matplotlib.pyplot as plt

    for rs in [4,69]:
        bn,finf = exact_constraints(rs,x_only=False)
        fig,ax = plt.subplots(figsize=(10,6))
        fxcu = frequency(rs,1.j*w,axis='imag',x_only=False,new_hx=False,use_par=False)
        fxcu_par = frequency(rs,1.j*w,axis='imag',x_only=False,new_hx=False,use_par=True)
        #fxcu_param = frequency(rs,1.j*w,axis='imag',x_only=False,new_hx=False,use_par=True)
        ax.plot(w,fxcu,linewidth=2)
        ax.plot(w,fxcu_par,linewidth=2)
        ax.hlines(finf,plt.xlim()[0],plt.xlim()[1],linestyle='--',color='gray')
        plt.title('$r_s=$'+str(rs),fontsize=20)
        ax.set_ylabel('$f_{\mathrm{xc}}(q=0,i \omega)$',fontsize=20)
        ax.set_xlabel('$\omega$',fontsize=20)
        ax.set_xlim([0,10.01])
        plt.savefig('./rs_'+str(rs)+'_analytic_continuation.pdf')
        plt.cla()
        plt.clf()
    exit()
    """
    #for iw,aw in enumerate(w):
    #    print(aw,fxcu[iw],fxcu_param[iw])
    exit()

    for z in np.arange(0.01,10,1):
        for w in np.arange(0.0,10,1):
            tw = w*1.j
            print(z,w,chi_mcp07(z,tw,4.0,ixn=0.0).real*(-pi**2/(9.0*pi/4.0)**(1.0/3.0)*4.0))
    exit()
    print(mcp07(1.e-6*np.ones(1),4.0)[0])
    print(frequency(4.0,1.e-6*np.ones(1))[0].real)
